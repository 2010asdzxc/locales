# Ayana Localization
>To be used with Ayana Discord Bot

[![Discord](https://discordapp.com/api/guilds/145166056812576768/embed.png)](https://discord.gg/WpfP3aW) [![Translation status](http://weblate.ayana.io/widgets/ayana/-/svg-badge.svg)](http://weblate.ayana.io/engage/ayana/?utm_source=widget)

## Contribute
If you would like to help translate Ayana to your locale [click here](http://weblate.ayana.io/engage/ayana/)

[![Translation status](http://weblate.ayana.io/widgets/ayana/-/multi-auto.svg)](http://weblate.ayana.io/engage/ayana/?utm_source=widget)